package com.infoedge.rgipassserverexample.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.infoedge.rgipassserverexample.models.RgiAnagraficaResponse;
import com.infoedge.rgipassserverexample.models.RgiAnagraficaResponsePojo;

@RestController
public class RgiServerController {

	public static RgiAnagraficaResponse RGI_RESPONSE = new RgiAnagraficaResponse();
	
	@PostMapping("/")
	public String rgiWelcomeTest() {
		return "Rgi server is working correctly";
	}
	
	@RequestMapping(
			value = "/RsGetPartiesByFilterRequestParam", 
			method = RequestMethod.POST, 
			produces = {MediaType.APPLICATION_JSON_VALUE })
	public RgiAnagraficaResponse postRGIUrlRequestParam (
			@RequestParam(value = "companyCode", required = false) String provider,
			@RequestParam(value = "fiscalCode", required = false) String compagnia,
			@RequestParam(value = "personTypeCode", required = false) String agenzia,
			@RequestParam(value = "policyRole", required = false) String tipoPersona,
			HttpServletRequest request) {

		RGI_RESPONSE.setCompanyCode(request.getParameter("companyCode"));
		RGI_RESPONSE.setFiscalCode(request.getParameter("fiscalCode"));
		RGI_RESPONSE.setPersonTypeCode(request.getParameter("personTypeCode"));
		RGI_RESPONSE.setPolicyRole(request.getParameter("policyRole"));

	
		return RGI_RESPONSE;
	}

	@RequestMapping(
			value = "/RsGetPartiesByFilterRequestPojo", 
			method = RequestMethod.POST,
			consumes = {MediaType.APPLICATION_JSON_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE })
	public RgiAnagraficaResponse postRGIUrlRequestBody(@RequestBody RgiAnagraficaResponsePojo rgiAnagraficaResponsePojo) {
		
		RGI_RESPONSE.setCompanyCode(rgiAnagraficaResponsePojo.companyCode);
		RGI_RESPONSE.setFiscalCode(rgiAnagraficaResponsePojo.fiscalCode);
		RGI_RESPONSE.setPersonTypeCode(rgiAnagraficaResponsePojo.personTypeCode);
		RGI_RESPONSE.setPolicyRole(rgiAnagraficaResponsePojo.policyRole);
		
		return RGI_RESPONSE;
	}
	
	
}
