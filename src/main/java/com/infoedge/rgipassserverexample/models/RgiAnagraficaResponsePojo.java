package com.infoedge.rgipassserverexample.models;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"companyCode",
"fiscalCode",
"personTypeCode",
"policyRole"
})
public class RgiAnagraficaResponsePojo {

	@JsonProperty("companyCode")
	public String companyCode;
	@JsonProperty("fiscalCode")
	public String fiscalCode;
	@JsonProperty("personTypeCode")
	public String personTypeCode;
	@JsonProperty("policyRole")
	public String policyRole;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	
	/**
	* No args constructor for use in serialization
	*
	*/
	public RgiAnagraficaResponsePojo() {
	}
	
	/**
	*
	* @param policyRole
	* @param fiscalCode
	* @param personTypeCode
	* @param companyCode
	*/
	public RgiAnagraficaResponsePojo(String companyCode, String fiscalCode, String personTypeCode, String policyRole) {
		super();
		this.companyCode = companyCode;
		this.fiscalCode = fiscalCode;
		this.personTypeCode = personTypeCode;
		this.policyRole = policyRole;
	}
	
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}
	
	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}


}
