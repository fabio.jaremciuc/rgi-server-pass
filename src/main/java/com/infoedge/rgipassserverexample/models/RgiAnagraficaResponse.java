package com.infoedge.rgipassserverexample.models;

public class RgiAnagraficaResponse {

	private String companyCode;
	private String fiscalCode;
	private String personTypeCode;
	private String policyRole;
	private String responseStatus = "OK";
	
	public RgiAnagraficaResponse() {
	}

	public RgiAnagraficaResponse(String companyCode, String fiscalCode, String personTypeCode, String policyRole,
			String responseStatus) {
		super();
		this.companyCode = companyCode;
		this.fiscalCode = fiscalCode;
		this.personTypeCode = personTypeCode;
		this.policyRole = policyRole;
		this.responseStatus = responseStatus;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getFiscalCode() {
		return fiscalCode;
	}

	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	public String getPersonTypeCode() {
		return personTypeCode;
	}

	public void setPersonTypeCode(String personTypeCode) {
		this.personTypeCode = personTypeCode;
	}

	public String getPolicyRole() {
		return policyRole;
	}

	public void setPolicyRole(String policyRole) {
		this.policyRole = policyRole;
	}

	public String getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}

	@Override
	public String toString() {
		return "RgiAnagraficaResponse [companyCode=" + companyCode + ", fiscalCode=" + fiscalCode + ", personTypeCode="
				+ personTypeCode + ", policyRole=" + policyRole + ", responseStatus=" + responseStatus + "]";
	}
	
}
